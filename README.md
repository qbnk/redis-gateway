# RedisGateway

This class is intended to add an abstraction layer between a PHP application and a Redis
setup that contains master and slave(s) instances which are monitored by RedisSentinels.

The idea is that this is exchangeable with a simple \Redis class, but also handles direction
to a new master instance in case of a node failure.


Before:
```php
$client = new Redis();
$client->connect('127.0.0.1', 6379, 0, null, 0, 0);

$client->auth('qwerty');
$client->select(123);
```

After:
```php
$client = new \QBNK\Redis\RedisGateway();

$client->addSentinels([
    new RedisSentinel('10.0.0.100', 26379),
    new RedisSentinel('10.0.0.101', 26379),
    new RedisSentinel('10.0.0.102', 26379)
]);

$client->auth('qwerty');
$client->select(123);
```

For setups where the Redis sentinels are not aware of the public IP addresses (eg virtual machines that are only aware
of internal IP addresses), you can add IP mappings to RedisGateway
```php
$alias1 = new \QBNK\Redis\Alias('127.0.0.1', '1.2.3.4', 1234, 5678);
$alias2 = new \QBNK\Redis\Alias('127.0.0.2', '1.2.3.5', 1235, 5679);


$client = new \QBNK\Redis\RedisGateway();
$client
    ->addAlias($alias1)
    ->addAlias($alias2);
```
