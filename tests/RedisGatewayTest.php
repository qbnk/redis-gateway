<?php
declare(strict_types=1);

use Mockery\LegacyMockInterface;
use Mockery\MockInterface;
use PHPUnit\Framework\TestCase;
use QBNK\Redis\Alias;
use QBNK\Redis\RedisGateway;
use QBNK\Redis\RedisHost;

class RedisGatewayTest extends TestCase {

	/** @var LegacyMockInterface|MockInterface|Redis */
	protected $redis;

	/** @var LegacyMockInterface|MockInterface|RedisSentinel */
	protected $sentinel;

	public function setUp(): void {
		parent::setUp();
		$this->redis = Mockery::mock(Redis::class);
		$this->redis->shouldReceive('connect')
			->with('127.0.0.1', 6379, 0, null, 0, 0.0)
			->once()
			->andReturn(true);

		$this->redis->shouldReceive('select')
			->with(123)
			->andReturn(true);

		$this->redis->shouldReceive('getDbNum')
			->andReturn(123);

		$this->sentinel = Mockery::mock(RedisSentinel::class);
		$this->sentinel->shouldReceive('masters')
			->andReturn([[
				'name' => 'redis-master-instance',
				'ip' => '127.0.0.1',
				'port' => '1234'
			]]);
		$this->sentinel->shouldReceive('getMasterAddrByName')
			->andReturn(['127.0.0.1', '1234']);
	}

	public function testClass(): void {
		$redisGateway = new RedisGateway();

		// RedisGateway will create a new Redis instance on connect if not already set.
		// Since we're mocking Redis with this test we need to set it before calling connect.
		$redisGateway->setRedis($this->redis);
		self::assertTrue($redisGateway->connect(
			'127.0.0.1',
			6379,
			0,
			null,
			0,
			0.0
		));

		self::assertTrue($redisGateway->select(123));
		self::assertEquals(123, $redisGateway->getDbNum());
	}

	public function testInternalMethod(): void {
		$redisGateway = new RedisGateway();
		$redisGateway->addSentinel($this->sentinel);
		$master = $this->getMaster($redisGateway);

		self::assertEquals('127.0.0.1', $master->ip);
		self::assertEquals(1234, (int)$master->port);
	}

	public function testInternalMethodWithAlias(): void {
		$alias = new Alias('127.0.0.1', '192.168.0.1', 1234, 5678);

		$redisGateway = new RedisGateway();
		$redisGateway
			->addSentinel($this->sentinel)
			->addAlias($alias);

		$master = $this->getMaster($redisGateway);

		self::assertEquals('192.168.0.1', $master->ip);
		self::assertEquals(5678, (int)$master->port);
	}

	/**
	 * Helper method that exposes private and protected methods
	 * @param string $class
	 * @param string $name
	 * @return ReflectionMethod
	 * @throws ReflectionException
	 */
	protected static function getMethod(string $class, string $name): \ReflectionMethod {
		$class = new ReflectionClass($class);
		$method = $class->getMethod($name);
		$method->setAccessible(true);
		return $method;
	}

	/**
	 * Helper method that returns the Redis master host reported by the sentinel
	 * @param RedisGateway $redisGateway
	 * @return RedisHost
	 * @throws ReflectionException
	 */
	protected function getMaster(RedisGateway $redisGateway): RedisHost {
		$method = self::getMethod(RedisGateway::class, 'resolveMaster');
		return $method->invokeArgs($redisGateway, []);
	}

	public function tearDown(): void {
		parent::tearDown();
		unset($this->redis, $this->sentinel);
	}
}
