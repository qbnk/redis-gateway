<?php
declare(strict_types=1);

namespace QBNK\Redis;

use BadMethodCallException;
use Redis;
use RedisException;
use RedisSentinel;
use Monolog\Logger;

/**
 * Class RedisGateway
 *
 * @see \Redis This class may be used as a proxy of the php-redis-extension.
 * @see RedisGateway::addSentinels() Use this if this class should resolve the master instance automatically
 * @see ResisGateway::setupRedisClient() You may setup a single redis client and skip the sentinels conf
 *
 *
 * RedisSentinel
 * @method array getMasterAddrByName(string $master_name)
 * @method array master(string $master_name)
 * @method array masters()
 * @method array sentinels(string $master_name)
 * @method array slaves(string $master_name)
 * @method boolean checkQuorum(string $master_name)
 * @method boolean ckquorum(string $master_name)
 * @method boolean failOver(string $master_name)
 * @method boolean flushConfig()
 * @method boolean monitor($master_name, $ip, $port, $quorum)
 * @method boolean remove($master_name)
 * @method boolean sentinelSet($master_name, $option, $value)
 * @method int reset(string $pattern)
 * @method string ping()
 *
 * Redis
 * @method bool select(int $dbIndex)
 * @method int|bool lPush(string $key, ...$value1)
 * @method int|bool rPush(string $key, ...$value1)
 * @method int|bool lPushx(string $key, $value)
 * @method int|bool rPushx(string $key, $value)
 * @method mixed|bool lPop(string $key)
 * @method mixed|bool rPop(string $key)
 * @method array blPop(string $keys, int $timeout)
 * @method array brPop(string $keys, int $timeout)
 * @method int|bool lLen(string $key)
 * @method mixed|bool lIndex(string $key, $index)
 * @method int|bool lGet(string $key)
 * @method int|bool lSet(string $key, int $index, string $value)
 * @method array lRange(string $key, int $start, int $end)
 * @method array|bool lTrim(string $key, int $start, int $stop)
 * @method int|bool lRem(string $key, string $value, int $count)
 * @method int lInsert(string $key, int $position, string $pivot, $value)
 * @method int|bool sAdd(string $key, ...$value1)
 * @method int sRem(string $key, ...$member1)
 * @method bool sMove(string $srcKey, string $dstKey, $member)
 * @method bool sIsMember(string $key, $value)
 * @method int sCard(string $key)
 * @method string|mixed|array|bool sPop(string $key, int $count)
 * @method string|mixed|array|bool sRandMember(string $key, int $count)
 * @method array sInter(string $key1, ...$otherKeys)
 * @method int|bool sInterStore(string $dstKey, string $key1, ...$otherKeys)
 * @method array sUnion(string $key1, ...$otherKeys)
 * @method int sUnionStore(string $dstKey, string $key1, ...$otherKeys)
 * @method array sDiff(string $key1, ...$otherKeys)
 * @method int sDiffStore(string $dstKey, string $key1, ...$otherKeys)
 * @method array sMembers(string $key)
 * @method array|bool sScan(string $key, int &$iterator, string $pattern = null, int $count = 0)
 * @method string|mixed getSet(string $key, $value)
 * @method string randomKey()
 * @method bool move(string $key, int $dbIndex)
 * @method bool rename(string $srcKey, string $dstKey)
 * @method bool renameNx(string $srcKey, string $dstKey)
 * @method bool expire(string $key, int $ttl)
 * @method bool pExpire(string $key, int $ttl)
 * @method bool expireAt(string $key, $timestamp)
 * @method bool pExpireAt(string $key, $timestamp)
 * @method array keys(string $pattern)
 * @method int dbSize()
 * @method bool auth(string $password)
 * @method bool bgrewriteaof()
 * @method bool slaveof(string $host = '127.0.0.1', int $port = 6379)
 * @method mixed slowLog(string $operation, int $length = null)
 * @method string|int|bool object(string $string = '', string $key = '')
 * @method bool save()
 * @method bool bgsave()
 * @method int lastSave()
 * @method int wait(int $numSlaves, int $timeout)
 * @method int type(string $key)
 * @method int append(string $key, $value)
 * @method string getRange(string $key, int $start, int $end)
 * @method int setRange(string $key, int $offset, int $value)
 * @method int strlen(string $key)
 * @method int bitpos(string $key, $bit, $start = 0, $end = null)
 * @method int getBit(string $key, $offset)
 * @method int setBit(string $key, $offset, $value)
 * @method int bitCount(string $key)
 * @method int bitOp($operation, $retKey, string $key1, ...$otherKeys)
 * @method true flushDB()
 * @method true flushAll()
 * @method array sort(string $key, $option = null)
 * @method string info($option = null)
 * @method bool resetStat()
 * @method int|bool ttl(string $key)
 * @method int|bool pttl(string $key)
 * @method true persist(string $key)
 * @method bool mset(array $array)
 * @method array mget(array $keys)
 * @method int msetnx(array $array)
 * @method string|mixed|bool rpoplpush($srcKey, $dstKey)
 * @method string|mixed|bool brpoplpush($srcKey, $dstKey, $timeout)
 * @method int zAdd(string $key, $options, $score1, $value1 = null, $score2 = null, $value2 = null, $scoreN = null, $valueN = null)
 * @method array zRange(string $key, $start, $end, $withscores = null)
 * @method int zRem(string $key, $member1, ...$otherMembers)
 * @method array zRevRange(string $key, $start, $end, $withscore = null)
 * @method array zRangeByScore(string $key, $start, $end, array $options = [])
 * @method array  zRevRangeByScore(string $key, $start, $end, array $options = [])
 * @method array|bool zRangeByLex(string $key, $min, $max, $offset = null, $limit = null)
 * @method array zRevRangeByLex(string $key, $min, $max, $offset = null, $limit = null)
 * @method int zCount(string $key, $start, $end)
 * @method int zRemRangeByScore(string $key, $start, $end)
 * @method int zRemRangeByRank(string $key, $start, $end)
 * @method int zCard(string $key)
 * @method float|bool zScore(string $key, $member)
 * @method int|bool zRank(string $key, $member)
 * @method int|bool zRevRank(string $key, $member)
 * @method float zIncrBy(string $key, $value, $member)
 * @method int zUnionStore($output, $zSetKeys, array $weights = null, $aggregateFunction = 'SUM')
 * @method int zInterStore($output, $zSetKeys, array $weights = null, $aggregateFunction = 'SUM')
 * @method array|bool zScan(string $key, &$iterator, $pattern = null, $count = 0)
 * @method array bzPopMax(string $key1, string $key2, $timeout)
 * @method array bzPopMin(string $key1, string $key2, $timeout)
 * @method array zPopMax(string $key, $count = 1)
 * @method array zPopMin(string $key, $count = 1)
 * @method int|bool hSet(string $key, $hashKey, $value)
 * @method bool hSetNx(string $key, $hashKey, $value)
 * @method string|bool hGet(string $key, $hashKey)
 * @method int|bool hLen(string $key)
 * @method int|bool hDel(string $key, $hashKey1, ...$otherHashKeys)
 * @method array hKeys(string $key)
 * @method string hVals(string $key)
 * @method array hGetAll(string $key)
 * @method bool hExists(string $key, $hashKey)
 * @method int hIncrBy(string $key, $hashKey, $value)
 * @method float hIncrByFloat(string $key, $field, $increment)
 * @method bool hMSet(string $key, $hashKeys)
 * @method array hMGet(string $key, $hashKeys)
 * @method array hScan(string $key, &$iterator, $pattern = null, $count = 0)
 * @method int hStrLen(string $key, string $field)
 * @method int geoadd(string $key, $longitude, $latitude, $member)
 * @method array geohash(string $key, ...$member)
 * @method array geopos(string $key, string $member)
 * @method float geodist(string $key, $member1, $member2, $unit = null)
 * @method mixed georadius(string $key, $longitude, $latitude, $radius, $unit, array $options = null)
 * @method array georadiusbymember(string $key, $member, $radius, $units, array $options = null)
 * @method array config($operation, string $key, $value)
 * @method mixed eval($script, $args = [], $numKeys = 0)
 * @method mixed evalSha($scriptSha, $args = [], $numKeys = 0)
 * @method mixed script($command, $script)
 * @method string|null getLastError()
 * @method true clearLastError()
 * @method mixed client($command, $value = '')
 * @method string _prefix($value)
 * @method mixed _unserialize($value)
 * @method mixed _serialize($value)
 * @method string|bool dump(string $key)
 * @method bool restore(string $key, $ttl, $value)
 * @method bool migrate($host, $port, string $key, $db, $timeout, $copy = false, $replace = false)
 * @method array time()
 * @method array|bool scan(&$iterator, $pattern = null, $count = 0)
 * @method bool pfAdd(string $key, array $elements)
 * @method int pfCount(string $key)
 * @method bool pfMerge($destKey, array $sourceKeys)
 * @method mixed rawCommand($command, $arguments)
 * @method int getMode()
 * @method int xAck($stream, $group, $messages)
 * @method string xAdd(string $key, $id, $messages, $maxLen = 0, $isApproximate = false)
 * @method array xClaim(string $key, $group, $consumer, $minIdleTime, $ids, $options = [])
 * @method int xDel(string $key, $ids)
 * @method mixed xGroup($operation, string $key, $group, $msgId = '', $mkStream = false)
 * @method mixed xInfo($operation, $stream, $group)
 * @method int xLen($stream)
 * @method array xPending($stream, $group, $start = null, $end = null, $count = null, $consumer = null)
 * @method array xRange($stream, $start, $end, $count = null)
 * @method array xRead($streams, $count = null, $block = null)
 * @method array xReadGroup($group, $consumer, $streams, $count = null, $block = null)
 * @method array xRevRange($stream, $end, $start, $count = null)
 * @method int xTrim($stream, $maxLen, $isApproximate)
 * @method int|bool sAddArray(string $key, array $values)
 * @method bool isConnected()
 * @method string|bool getHost()
 * @method string|bool getPort()
 * @method int|bool getDbNum()
 * @method float|bool getTimeout()
 * @method float|bool getReadTimeout()
 * @method string|null|bool getPersistentID()
 * @method string|null|bool getAuth()
 * @method bool close()
 * @method bool swapdb(int $db1, int $db2)
 * @method bool setOption($option, $value)
 * @method mixed|null getOption($option)
 * @method string echo(string $message)
 * @method string|mixed|bool get(string $key)
 * @method bool set(string $key, $value, $timeout)
 * @method bool setex(string $key, $ttl, $value)
 * @method bool psetex(string $key, $ttl, $value)
 * @method bool setnx(string $key, $value)
 * @method int del(string $key1, ...$otherKeys)
 * @method int unlink(string $key1, string $key2 = null, string $key3 = null)
 * @method Redis multi($mode = Redis::MULTI)
 * @method Redis pipeline()
 * @method void|array exec()
 * @method discard() @see Redis::multi()
 * @method void watch(string $key)
 * @method unwatch() @see Redis::watch()
 * @method mixed|null subscribe($channels, $callback)
 * @method mixed psubscribe(array $patterns, $callback)
 * @method int publish(string $channel, string $message)
 * @method array|int pubsub(string $keyword, $argument)
 * @method unsubscribe(array $channels = null)
 * @method punsubscribe($patterns = null)
 * @method int|bool exists(string $key)
 * @method int incr(string $key)
 * @method float incrByFloat(string $key, float $increment)
 * @method int incrBy(string $key, int $value)
 * @method int decr(string $key)
 * @method int decrBy(string $key, int $value)
 *
 * TODO: add more method annotations when needed
 */
class RedisGateway {

	/** @var RedisSentinel[] */
	protected $sentinels;

	/** @var Logger */
	protected $logger;

	/** @var Redis */
	protected $redis;

	/** @var int */
	protected $maxAttempts;

	/** @var Alias[] */
	protected $aliases;

	/**
	 * RedisGateway constructor.
	 * @param Logger|null $logger
	 */
	public function __construct(?Logger $logger = null) {
		$this->sentinels = [];
		$this->aliases = [];
		$this->maxAttempts = 0;

		if ($logger instanceof Logger) {
			$this->logger = $logger;
		}
	}

	public function setPassword(string $password): RedisGateway {
		$this->password($password);
		return $this;
	}

	/**
	 * @internal hack that makes password somewhat invisible in case of var_dumps
	 * @param string|null $setPassword
	 * @return string|null
	 */
	protected function password(string $setPassword = null):? string {
		static $password;
		if (null !== $setPassword) {
			$password = $setPassword;
		}

		return $password;
	}

	/**
	 * @param RedisSentinel[] $sentinels
	 * @return $this
	 */
	public function addSentinels(array $sentinels): RedisGateway {
		foreach($sentinels as $sentinel) {
			$this->sentinels[] = $sentinel;
		}
		return $this;
	}

	/**
	 * @param RedisSentinel $sentinel
	 * @return $this
	 */
	public function addSentinel(RedisSentinel $sentinel): RedisGateway {
		$this->sentinels[] = $sentinel;

		return $this;
	}

	/**
	 * May be used if we don't have any sentinels in our setup
	 *
	 * @param Redis $redis
	 * @return $this
	 */
	public function setRedis(Redis $redis): RedisGateway {
		$this->redis = $redis;
		return $this;
	}

	/**
	 * @return Redis
	 * @throws RedisException
	 */
	public function getRedis():? Redis {
		if ($this->redis instanceof Redis) {
			return $this->redis;
		}

		if (empty($this->sentinels)) {
			return null;
		}

		$master = $this->resolveMaster();
		if ($master === null) {
			return null;
		}

		$this->connect($master->ip, (int)$master->port);
		return $this->redis;
	}

	/**
	 * @return RedisHost
	 * @throws RedisGatewayException
	 */
	protected function resolveMaster():? RedisHost {

		if (empty($this->sentinels)) {
			return null;
		}

		$masters = array_map(static function (array $master) {
			return new RedisHost($master);
		}, $this->masters());


		if (!$masters) {
			throw new RedisGatewayException("Unable to retrieve the master redis");
		}

		$this->log('Redis master found, resolving address...', Logger::DEBUG);

		/** @var $address = [
		 *  0 => string,    IP
		 *  1 => string     Port
		 * ]
		 */
		$address = $this->getMasterAddrByName($masters[0]->name);
		$resolvedHost = null;
		foreach ($masters as $master) {
			if ($master->ip === $address[0]) {
				$resolvedHost = $master;
			}
		}

		if (isset($this->aliases[$address[0]])) {
			$alias = $this->aliases[$address[0]];

			$resolvedHost->ip = $alias->getAliasIp() ?? $address[0];
			$resolvedHost->port = $alias->getAliasPort() ?? $address[1];
		}

		$this->log(sprintf('Redis master resolved on %s:%s', $resolvedHost->ip, $resolvedHost->port), Logger::DEBUG);

		return $resolvedHost;
	}

	/**
	 * @param string $ip
	 * @param int $port
	 * @param float $timeout
	 * @param null $reserved
	 * @param int $retryInterval
	 * @param float $readTimeout
	 * @return bool
	 * @throws RedisGatewayException
	 */
	public function connect(
		string $ip,
		int $port = 6379,
		float $timeout = 0.0,
		$reserved = null,
		int $retryInterval = 0,
		float $readTimeout = 0.0)
	: bool {
		if ($this->redis === null) {
			$this->redis = new Redis();
		}

		if (!$this->redis->connect($ip, $port, $timeout, (string)$reserved, $retryInterval, $readTimeout)) {
			throw new RedisGatewayException(sprintf('Connect to Redis failed on %s:%d', $ip, $port));
		}

		if (!empty($this->password())) {
			$redisAuthStatus = $this->redis->auth($this->password());
			if ($redisAuthStatus === false) {
				$this->log('Wrong authentication for Redis. Please check the configuration.', Logger::CRITICAL);
				throw new RedisGatewayException('Wrong authentication for Redis.');
			}
		}

		return true;
	}

	public function __call($name, $arguments) {

		switch (mb_strtolower($name)) {
			case mb_strtolower('sentinelSet'):
				$name = 'set';
				break;
			case 'auth':
				if (!empty($arguments[0])) {
					$this->setPassword($arguments[0]);
				}
				break;
			default:
				// Do nothing
		}

		foreach ($this->sentinels as $sentinel) {
			if (!method_exists($sentinel, $name)) {
				break;
			}

			try {
				return call_user_func_array([$sentinel, $name], $arguments);
			} catch (\Throwable $t) {
				$this->log($t->getMessage(), Logger::DEBUG, $t);
				continue;
			}
		}

		try {
			if ($this->getRedis() instanceof Redis) {
				if (!method_exists($this->redis, $name)) {
					throw new BadMethodCallException(sprintf('Method "%s" does not exists.', $name));
				}

				$attempts = 1;
				do {
					try {
						return call_user_func_array([$this->redis, $name], $arguments);
					} catch (\Throwable $t) {
						$this->log($t->getMessage(), Logger::ERROR, $t);
						if (empty($this->sentinels)) {
							throw $t;
						}
					}

					if ($this->maxAttempts !== 0 && $this->maxAttempts < $attempts) {
						break;
					}

					$this->log(sprintf(
						'RedisGateway tried to call %s, master instance might have gone down, trying to resolve a new master with %d sentinels. Attempt %d%s.',
						$name,
						count($this->sentinels),
						$attempts,
						($this->maxAttempts > 0 ? ' of ' . $this->maxAttempts : '')
					), Logger::DEBUG);

					sleep((int) floor(1.5 * $attempts));
					$redisHost = $this->resolveMaster();
					$attempts++;

					if ($redisHost === null) {
						continue;
					}

					$this->connect($redisHost->ip, (int)$redisHost->port);

				} while (true);
			}
		} catch (RedisException $e) {
			$this->log(sprintf('RedisGateway: Redis exception thrown %s; %s', $e->getMessage(), $e->getTraceAsString()), Logger::ERROR, $e);
		} catch (BadMethodCallException $bmce) {
			$this->log(sprintf('RedisGateway: %s; %s', $bmce->getMessage(), $bmce->getTraceAsString()), Logger::ERROR, $bmce);
		} catch (\Throwable $t) {
			$this->log(sprintf('RedisGateway: An undefined error occurred. %s; %s', $t->getMessage(), $t->getTraceAsString()), Logger::ERROR, $t);
		}

		throw new RedisGatewayException(sprintf('Unable to proxy method "%s" to any redis service.', $name));
	}

	private function log(string $message, int $severity = Logger::ERROR, \Throwable $t = null): RedisGateway {
		if ($this->logger instanceof Logger) {
			$this->logger->log($severity, $message, $t instanceof \Throwable ? $t->getTrace() : []);
		} else {
			error_log($message);
		}

		return $this;
	}

	/**
	 * @return Alias[]
	 */
	public function getAliasesMappings(): array {
		return $this->aliases;
	}

	/**
	 * @param Alias[] $aliases
	 * @return RedisGateway
	 */
	public function setAliases(array $aliases): RedisGateway {
		foreach ($aliases as $alias) {
			$this->addAlias($alias);
		}

		return $this;
	}

	/**
	 * @param Alias $alias
	 * @return RedisGateway
	 */
	public function addAlias(Alias $alias): RedisGateway {
		$this->aliases[$alias->getReportedIp()] = $alias;

		return $this;
	}

	/**
	 * @param int $maxAttempts
	 * @return RedisGateway
	 */
	public function setMaxAttempts(int $maxAttempts): RedisGateway {
		$this->maxAttempts = $maxAttempts;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getMaxAttempts(): int {
		return $this->maxAttempts;
	}
}
