<?php
declare(strict_types=1);

namespace QBNK\Redis;


class Alias {

	/** @var string - the IP reported by the sentinel */
	protected $reportedIp;

	/** @var int - the port reported by the sentinel */
	protected $reportedPort;

	/** @var string - the IP to be used when connecting to the Redis service */
	protected $aliasIp;

	/** @var int - the IP to be used when connecting to the Redis service */
	protected $aliasPort;

	public function __construct(string $reportedIp, string $aliasIp, ?int $reportedPort = null, ?int $aliasPort = null) {
		$this->setReportedIp($reportedIp);
		$this->setAliasIp($aliasIp);

		if ($reportedPort !== null) {
			$this->setReportedPort($reportedPort);
		}

		if ($aliasPort !== null) {
			$this->setAliasPort($aliasPort);
		}
	}

	public function getReportedIp(): string {
		return $this->reportedIp;
	}

	public function setReportedIp(string $reportedIp): Alias {
		$this->reportedIp = $reportedIp;
		return $this;
	}

	public function getReportedPort():? int {
		return $this->reportedPort;
	}

	public function setReportedPort(int $reportedPort): Alias {
		$this->reportedPort = $reportedPort;
		return $this;
	}

	public function getAliasIp(): string {
		return $this->aliasIp;
	}

	public function setAliasIp(string $aliasIp): Alias {
		$this->aliasIp = $aliasIp;
		return $this;
	}

	public function getAliasPort():? int {
		return $this->aliasPort;
	}

	public function setAliasPort(int $aliasPort): Alias {
		$this->aliasPort = $aliasPort;
		return $this;
	}
}
