<?php


namespace QBNK\Redis;


class RedisHost {

	public $name;
	public $ip;
	public $port;
	public $flags;
	public $linkPendingCommands;
	public $runId;
	public $linkRefCount;
	public $lastPingSent;
	public $lastOkPingReply;
	public $lastPingReply;
	public $downAfterMilliseconds;
	public $infoRefresh;
	public $roleReported;
	public $roleReportedTime;
	public $configEpoch;
	public $numSlaves;
	public $numOtherSentinels;
	public $quorum;
	public $failOverTimeout;
	public $parallelSyncs;

	/**
	 * RedisHost constructor.
	 * @param $params = [
	 *     name => string,
	 *     ip => string,
	 *     port => string
	 *     runid => string
	 *     flags => string
	 *     link-pending-commands => string
	 *     link-refcount => string
	 *     last-ping-sent => string
	 *     last-ok-ping-reply => string
	 *     last-ping-reply => string
	 *     down-after-milliseconds => string
	 *     info-refresh => string
	 *     role-reported => string
	 *     role-reported-time => string
	 *     config-epoch => string
	 *     num-slaves => string
	 *     num-other-sentinels => string
	 *     quorum => string
	 *     failover-timeout => string
	 *     parallel-syncs => string
	 * ]
	 */
	public function __construct(array $params = []) {
		$this->name = $params['name'] ?? null;
		$this->ip = $params['ip'] ?? null;
		$this->port = $params['port'] ?? null;
		$this->runId = $params['runid'] ?? null;
		$this->flags = $params['flags'] ?? null;
		$this->linkPendingCommands = $params['link-pending-commands'] ?? null;
		$this->linkRefCount = $params['link-refcount'] ?? null;
		$this->lastPingSent = $params['last-ping-sent'] ?? null;
		$this->lastOkPingReply = $params['last-ok-ping-reply'] ?? null;
		$this->lastPingReply = $params['last-ping-reply'] ?? null;
		$this->downAfterMilliseconds = $params['down-after-milliseconds'] ?? null;
		$this->infoRefresh = $params['info-refresh'] ?? null;
		$this->roleReported = $params['role-reported'] ?? null;
		$this->roleReportedTime = $params['role-reported-time'] ?? null;
		$this->configEpoch = $params['config-epoch'] ?? null;
		$this->numSlaves = $params['num-slaves'] ?? null;
		$this->numOtherSentinels = $params['num-other-sentinels'] ?? null;
		$this->quorum = $params['quorum'] ?? null;
		$this->failOverTimeout = $params['failover-timeout'] ?? null;
		$this->parallelSyncs = $params['parallel-syncs'] ?? null;
	}
}
